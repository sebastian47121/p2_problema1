#include <iostream>

using namespace std;

int main()
{

    int arreglo[10]={50000,20000,10000,5000,2000,1000,500,200,100,50};
    int cantidad,valor,aux;

    cout << "Ingrese el valor del dinero para darla en valores en combinacion de billetes y monedas: " << endl;
    cin >> cantidad;


    for (int i=0;i<10;i++){
        valor = arreglo[i]; //Recorre el valor de cada moneda por el arreglo

        aux=cantidad/valor; //Verifica cuantas veces se puede dar ese valor en la cantidad total
        cantidad=cantidad-(aux*valor); //Le elimina el valor cuantas veces sea posible al total
        cout<<"$"<<valor<<":"<<aux<<endl;

    }
    cout <<"Restante: "<<cantidad<<endl;


    return 0;
}
